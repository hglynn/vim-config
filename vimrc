 " General {
    set nocompatible
    set vb t_vb=
    set guifont=Monaco\ 9
    color molokai
    set directory=~/.vimtemp
    syntax on
    filetype plugin indent on " load filetype plugins/indent settings
    set nocp
    set ofu=syntaxcomplete#Complete
    set title
    set history=1000
    set undolevels=1000
    set noswapfile
" }

" Vim UI {
    set guioptions-=T
    set mouse=a
    set ttymouse=xterm
    set number " turn on line numbers
    set showmatch
" }

" Text Formatting/Layout {
    set expandtab
    set tabstop=8
    set softtabstop=4
    set shiftwidth=4
    set autoindent
    :syntax on
" }

" Folding {
    set nofoldenable " Turn off folding
" }

" Line numbers, turn them on, and use F2 as a toggle
nnoremap <F3> :set nonumber!<CR>:set foldcolumn=0<CR>

" NERDtree buffer explorer related
nmap <F2> :NERDTreeToggle<CR>
map <F2> :NERDTreeToggle<CR>
let NERDTreeIgnore=['\.pyc', '\~$']
let NERDTreeShowBookmarks=1

" Cycle thru the buffers with Ctrl-n, p
:nnoremap <C-n> :bnext<CR>
:nnoremap <C-p> :bprevious<CR>

" Top line mini buffer explorer settings
let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1 
